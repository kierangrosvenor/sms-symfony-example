# SMS Example

 - Messages are limited to 1 per 15 seconds 
 - Authentication via username, password
 - Transmit text messages via Twilio API
 - Using Symfony 4, Twig, Bootstrap 4,

## Getting started

Configuration of RabbitMQ, Twilio has been handled by me, I've done this in the **.env** file, so only the DATABASE_URL will need setting.

The database will need migrating

    php bin/console make:migration
    php bin/console doctrine:migrations:migrate
   

## Running the SMS message consumer 
    php bin/console rabbitmq:consumer sms_message
    
# Notes

**Navigate to /public/**

I added the .htaccess to /public/ to remove index.php, you will need to navigate to there. I'm running XAMPP so i thought  it would be simpler to leave the default.