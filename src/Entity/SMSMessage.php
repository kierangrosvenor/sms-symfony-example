<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SMSMessageRepository")
 */
class SMSMessage
{
    public function __construct()
    {
        $this->status = "queued";
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    private $userId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $failedAt;

    /**
     * @ORM\Column(type="string", columnDefinition="enum('queued', 'sent', 'failed')", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="text")
     */
    private $telephone;

    /**
     * @ORM\Column(type="text")
     */
    private $message;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(User $user): self {
        $this->userId = $user;
        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;
        return $this;
    }

    public function getStatus(): ?string {
       return $this->status;
    }


    public function setFailedAt(\DateTime $failedAt): self {
        $this->failedAt = $failedAt;
        return $this;
    }

    public function setStatus(string $status): self {
        $this->status = $status;
        return $this;
    }

    public function setSentAt(\DateTime $sentAt) : self {
        $this->sentAt = $sentAt;
        return $this;
    }

    public function getSentAt() : \DateTime {
        return $this->sentAt;
    }

    public function getCreatedAt() : \DateTime {
        return  $this->createdAt;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }
}
