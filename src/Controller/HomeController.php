<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 22/01/2019
 * Time: 14:34
 */

namespace App\Controller;
use App\Entity\SMSMessage;
use App\Form\SMSMessageFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;

class HomeController   extends Controller
{
    /**
     * @Route("/", name="home")
     * @Method({"GET"})
     * @RateLimit(methods={"POST"}, limit=1, period=15)
     */
    public function index(Request $request) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $smsMessage = new SMSMessage();
        $smsMessage->setUserId($user);

        $form  = $this->createForm(SMSMessageFormType::class, $smsMessage);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($smsMessage);
            $entityManager->flush();
            $message = json_encode([
                "type" =>   "SMSMessage",
                "data" =>  [
                    "id" => $smsMessage->getId(),
                    'user'=> $smsMessage->getUserId()->getId(),
                    "message" => $smsMessage->getMessage(),
                    "telephone" => $smsMessage->getTelephone()
                ]
            ]);
            $this->get('old_sound_rabbit_mq.sms_message_producer')->setContentType('application/json');
            $this->get('old_sound_rabbit_mq.sms_message_producer')->publish($message);
            $this->addFlash('success', "SMS message was dispatched");
            return $this->redirectToRoute("home");
        }

        return $this->render('home/index.html.twig', [
            'pagination' => $this->paginateSMS($request),
            'user' => $user,
            'form' => $form->createView()
        ]);
    }


    private function paginateSMS(Request $request)  {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM App\Entity\SMSMessage a WHERE a.userId = :userId ORDER BY a.createdAt DESC";
        $query = $em->createQuery($dql);
        $query->setParameter('userId', $this->getUser());
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1), 10
        );
        return $pagination;
    }




}