<?php

namespace App\Form;
use App\Entity\SMSMessage;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class SMSMessageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('telephone', TextType::class)
            ->add('message', TextareaType::class, [
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Message cannot be empty',
                    ]),
                    new Length([
                        'min' => 1,
                        'minMessage' => 'Your message must not be empty',
                        'maxMessage' => 'Message cannot exceed 140 characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 140,
                    ]),
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SMSMessage::class,
        ]);
    }
}
