<?php

namespace App\Repository;

use App\Entity\SMSMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SMSMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method SMSMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method SMSMessage[]    findAll()
 * @method SMSMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SMSMessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SMSMessage::class);
    }

    // /**
    //  * @return SMSMessage[] Returns an array of SMSMessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOne($id): ?SMSMessage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
