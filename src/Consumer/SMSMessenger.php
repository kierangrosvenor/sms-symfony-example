<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 22/01/2019
 * Time: 21:02
 */

namespace App\Consumer;

use App\Entity\SMSMessage;
use App\Entity\User;
use App\Repository\SMSMessageRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Twilio\Rest\Client;


class SMSMessenger implements ConsumerInterface
{
    /**
     * @var EntityManagerInterface $_em
     */
    private $_em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->_em = $entityManager;
    }

    public function execute(AMQPMessage $msg)
    {
        echo $msg->body . PHP_EOL;

        $message = json_decode($msg->body, true);
        echo "New sms message";

        $type = $message["type"];
        if ($type == "SMSMessage") $this->prepare($message['data']);
    }

    private function send(SMSMessage $smsMessage)
    {
        //should be environment variables

        $account_sid = getenv('TWILIO_ACCOUNT_ID');
        $auth_token =  getenv('TWILIO_AUTH_TOKEN');
        $twilio_number =  getenv('TWILIO_ACCOUNT_NUMBER');

        $client = new Client($account_sid, $auth_token);
        try {
            $client->messages->create(
                $smsMessage->getTelephone(),
                array(
                    'from' => $twilio_number,
                    'body' => $smsMessage->getMessage()
                )
            );
            $smsMessage->setSentAt(new \DateTime());
            $smsMessage->setStatus('sent');
            $this->_em->persist($smsMessage);
            $this->_em->flush();
            echo '[x] Sent message' .PHP_EOL;

        } catch (\Exception $ex) {
            echo "[x] Failed to send" . PHP_EOL;
            $smsMessage->setFailedAt(new \DateTime());
            $smsMessage->setStatus("failed");
            $this->_em->persist($smsMessage);
            $this->_em->flush();
        }
    }

    private function prepare($message)
    {
        $sms = $this->_em->getRepository(SMSMessage::class)->find($message['id']);
        if ($sms) {
            $this->send($sms);
        }
    }
}